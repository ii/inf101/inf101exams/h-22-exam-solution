package inf101h22.university;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class UniversityClass implements IClass {

    private List<Student> students = new ArrayList<>();

    @Override
    public double getAverageGpa() {
        if (this.size() == 0) {
            return 0;
        }
        double sumGpa = 0;
        for (Student student : this) {
            sumGpa += student.getGrades().getGpa();
        }
        return sumGpa / this.size();
    }

    @Override
    public Iterator<Student> iterator() {
        return this.students.iterator();
    }

    @Override
    public int size() {
        return this.students.size();
    }

    @Override
    public boolean add(Student student) {
        this.students.add(student);
        return true;
    }
}
